import 'package:flutter/material.dart';
import 'package:sample_form/entities/form.dart';
import 'package:sample_form/widgets/item-list.dart';

class ListPage extends StatefulWidget {

  @override _ListPageState createState() => _ListPageState();

}

class _ListPageState extends State<ListPage> {

  List<CustomForm> forms = [];

  @override void initState() {

    // Генерим формы
    for(int i = 0; i < 10; i++) forms.add(CustomForm.generic());

    super.initState();
  }

  @override Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Мега список'),
      ),
      body: ListView.builder( //Создаём виджет для отображения формы
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16), //Задаём отступы, параметр не обязательный
        physics: BouncingScrollPhysics(), //Задаём физику, параметр не обязательный
        itemCount: forms.length, //Обязательный параметр, если не указать то ListView выыйдет за пределы массива, что вызовет исключение
        itemBuilder: (BuildContext context, int pos) => ItemList(forms[pos]), //Создам свой элемент, тут pos - это порядковый номер элемента в массиве, а context не используется
      ),
    );
  }

}