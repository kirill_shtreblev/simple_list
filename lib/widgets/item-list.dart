import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:sample_form/entities/form-type.dart';
import 'package:sample_form/entities/form.dart';

class ItemList extends StatefulWidget {

  //Принимаем форму
  final CustomForm form;

  ItemList(this.form);

  @override _ItemListState createState() => _ItemListState();
}

class _ItemListState extends State<ItemList> {

  @override Widget build(BuildContext context) {
    //Создаём саму форму
    return Card(
      child: Padding( //отступ внутри Card
          padding: EdgeInsets.all(12),
          child: ExpansionTile( //Widget позволяет сделать выпадающий список
            title: Text(widget.form.title, style: TextStyle(fontSize: 18),),
            subtitle: Text(widget.form.subtitle, style: TextStyle(fontSize: 14, color: Colors.black.withOpacity(.8)),),
            children: widget.form.forms.map<Widget>((SubForm sub) => parseSubForm(sub)).toList(), //превращаем map<SubForm, Widget> в List, так как Widget в children принимает List<Widget>
            //более простая форма children: <Widget>[...]
          )
      )
    );
  }

  //Генерация элемента формы, всё просто через switch в зависимости от типа лепим кастомную форму
  Widget parseSubForm(SubForm subForm) {
    switch(subForm.type) {
      case FormType.INPUT_TEXT:
        //Для ввода текста создаём контроллер так как по другому не задать изначальное значение
        TextEditingController ctrl = TextEditingController();
        ctrl.text = subForm.value;
        return Column( //Widget для создание колонны
          crossAxisAlignment: CrossAxisAlignment.start, //Выравниваем содержимое по оси X
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 24),), //Паддинг что бы всё не слиплось
            Text(subForm.title, style: TextStyle(fontWeight: FontWeight.bold),),
            TextField(
              controller: ctrl,
              onChanged: (String value) => subForm.value = value, //Обновляем значение CustomForm с каждым изменением значения в водимой части
            ),
          ],
        );
      case FormType.DATE_PICKER:
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 24),),
            InkWell( //Widget понимающий нажатия
              child: Card(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Row(//Widget для создания строки
                    mainAxisAlignment: MainAxisAlignment.spaceBetween, //Выравнимание по краям
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Text(subForm.title, style: TextStyle(fontWeight: FontWeight.bold),), // Вывод тайтла для SubForm
                          Text(subForm.value), // Вывод текущей даты
                        ],
                      ),
                      Icon(Icons.timer)
                    ],
                  ),
                ),
              ),
              onTap: () {
                //По клику на SubForm выводим дайт пикер
                DatePicker.showDatePicker(
                  context,
                  theme: DatePickerTheme(
                    containerHeight: 210.0,
                  ),
                  showTitleActions: true, onConfirm: (time) {
                  print('confirm $time');
                  //Изменяем значение в SubForm
                  subForm.value = '${time.year}-${time.month}-${time.day}';
                  // обновляем виджет, иначе значение на экране не поменяется
                  setState(() {});
                },
                  currentTime: DateTime.utc(num.parse(subForm.value.split('-').first), num.parse(subForm.value.split('-')[1]),
                      num.parse(subForm.value.split('-').last)),
                  locale: LocaleType.ru,
                );
                setState(() {});
              },
            )
          ],
        );
      case FormType.IMAGE_LIST:
        List<Widget> images = [];
        // генерим статические виджеты, можно способом как с массивом SubForm, меньше строк будет
        for(String url in subForm.value) {
          images.add(ClipRRect( // делаем картинку круглой
              borderRadius: BorderRadius.circular(56),
              child: CachedNetworkImage( // Widget для загрузки фото с тырнета
                height: 56,
                width: 56,
                imageUrl: url,
              )));
        }
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 24)),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(subForm.title, style: TextStyle(fontWeight: FontWeight.bold),),
                Row(children: images)
              ],
            )
          ],
        );
      //Тут простой вывод текста
      default: return Padding(
        padding: EdgeInsets.only(top: 24),
        child: Row(
          children: <Widget>[
            RichText( //Умеет комбинировать тексты разных стилей
              text: TextSpan(
                  text: '${subForm.title}: ',
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14, color: Colors.black),
                  children: <TextSpan> [
                    TextSpan(
                        text: subForm.value,
                        style: TextStyle(fontWeight: FontWeight.normal, fontSize: 13)
                    )
                  ]
              ),
            )
          ],
        ),
      );
    }
  }

}