enum FormType  {
  INPUT_TEXT,
  PRINT_TEXT,
  DATE_PICKER,
  IMAGE_LIST
}

FormType numToFormType(int numType) {
  switch(numType) {
    case 0: return FormType.INPUT_TEXT;
    case 1: return FormType.PRINT_TEXT;
    case 2: return FormType.DATE_PICKER;
    case 3: return FormType.IMAGE_LIST;
    default: return FormType.PRINT_TEXT;
  }
}