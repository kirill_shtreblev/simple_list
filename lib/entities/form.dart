import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:sample_form/entities/form-type.dart';

class CustomForm {
  String title;
  String subtitle;

  List<SubForm> forms;

  /*
   * Класс описывающий форму
   * title - заголовок
   * subtitle - подзаголовок
   * forms - элементы формы ввод/вывод текста, дэйт пикер или картинки
   */
  CustomForm({
    @override this.title,
    String subtitle,
    @override this.forms
  }) :
      assert(title != '' && forms != null),
      subtitle = subtitle == null ? '' : subtitle;


  //Генерация форм для демонстрации
  static CustomForm generic() {
    int lengthForms = Random().nextInt(10)+1;
    List<SubForm> subForms = [];
    for(int i=0; i < lengthForms; i++) {
      subForms.add(_generic(Random().nextInt(5)));
    }
    return CustomForm(
      title: '${Random().nextInt(50) < 25 ? 'Супер' : 'Простая'} форма',
      subtitle: '${Random().nextInt(50) < 25 ? 'Совершенно секретно' : 'Можно и не заполнять'}',
      forms: subForms,
    );
  }

  //Генерация элементов формы для демонстрации
  static SubForm _generic(int numType) {
    FormType type = numToFormType(numType);

    switch(type) {
      case FormType.INPUT_TEXT:
        return SubForm<String>(
          type: FormType.INPUT_TEXT,
          title: 'Ввод текста',
          value: 'val ${Random().nextInt(1000)}',
        );
      case FormType.PRINT_TEXT:
        return SubForm<String>(
          type: FormType.PRINT_TEXT,
          title: 'Вывод текста',
          value: 'Object state: ${Random().nextInt(1000)}',
        );
      case FormType.DATE_PICKER:
        return SubForm<String>(
          type: FormType.DATE_PICKER,
          title: 'Пикер даты',
          value: '2020-10-12'
        );
      case FormType.IMAGE_LIST:
        return SubForm<List<String>>(
          type: FormType.IMAGE_LIST,
          title: 'Уже в наряде',
          value: [
            'https://avatars.mds.yandex.net/get-districts/1523149/2a0000016e1c5de17103d3f3c5ac6bd4a95f/optimize',
            'https://avatars.mds.yandex.net/get-districts/1523149/2a0000016e1c5de17103d3f3c5ac6bd4a95f/optimize',
            'https://avatars.mds.yandex.net/get-districts/1523149/2a0000016e1c5de17103d3f3c5ac6bd4a95f/optimize'
          ]
        );
    }
  }
}

/*
 * SubForm описывает вложенные структуры в Form
 * type - тип формы ввод/вывод текста, дайт пикер, список фото
 * title - заголовок
 * T - динамичская переменная
 */
class SubForm<T> {
  FormType type;

  String title;
  T value;

  SubForm({
    @required this.type,
    String title,
    @required this.value
  }) : assert(type != null && value != null),
       title = title == null ? '' : title;
}